@extends('the_pit::layouts/page')
@section('css')
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/news.css')}}">
@endsection
@section('page')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="page_title_main">{{trans('the_pit::main.menu_news')}}</h1>
            </div>
            <div class="col-xs-12">
                <ul class="filters list-inline" style="z-index: 1000;">
                    <li class="active">
                        <a href="{{route('site:news')}}" class="filter-button">{{trans('the_pit::main.news_all')}}</a>
                    </li>
                    <li>
                        <div class="filter" id="filter-list">
                            {!! Form::open(['url' => route('site:news'), 'class' => 'form-inline', 'enctype' => 'multipart/form-data','method'=>'GET']) !!}
                            {!! Form::select('category',$categories,isset($_GET['category'])?$_GET['category']:'',['class' => 'form-control',
                            'placeholder'=>trans('the_pit::main.news_category_select')]) !!}
                            {!! Form::text('filter',isset($_GET['filter'])?$_GET['filter']:'',['class'=>'form-control']) !!}
                            {!! Form::submit(trans('the_pit::main.news_find_button'), ['class'=>'']) !!}
                            {!! Form::close() !!}
                        </div>
                    </li>
                    <li class="active">
                        <span id="filter" class="filter-button">{{trans('the_pit::main.news_filter')}}</span>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                @if(isset($collection))
                    <div class="grid" id="articles">
                        @include('the_pit::layouts/block/popular')
                        @if($collection->count())
                            @foreach($collection as $article)
                                @include('the_pit::layouts/block/regular')
                            @endforeach
                        @else
                            <span class="no-data">No data</span>
                        @endif
                    </div>
                    {!! $collection->render() !!}
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{$app['the_pit.assets']->getPath('/js/masonry.pkgd.min.js')}}"></script>
    <script src="{{$app['the_pit.assets']->getPath('/js/imagesloaded.pkgd.min.js')}}"></script>
    <script>
        function croper() {
            $.each($(".grid-item"), function (k, v) {
                $(v).width($(v).width() - 20);
            });
        }
        isotope = function () {
            croper();
            momentoForDates();
            var $grid = $("#articles").masonry({
                percentPosition: true,
                transitionDuration: '0.2s',
                gutter: 5
            });
            $grid.imagesLoaded().progress(function () {
                $grid.masonry('layout');
            });
        };
        $(document).ready(function () {
            isotope();
            $('#filter').on('click', function () {
                $('#filter-list').slideToggle(200)
                $(this).hide();
            });
        });
    </script>
@endsection