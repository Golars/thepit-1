<!DOCTYPE html>
<!-- saved from url=(0028)http://dev1.jabber-kiev.com/ -->
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- TITLE OF SITE -->
    <title>@yield('title',"Main Page")</title>
    <!-- META DATA -->
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="metrothemes">
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <!-- =========================
      FAV AND TOUCH ICONS
    ============================== -->
    <!-- BOOTSTRAP CSS -->
    <link href="{{$app['pie_base.assets']->getPath('/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- FONT ICONS CSS -->
    <link href="{{$app['pie_base.assets']->getPath('/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="icon" href="@yield('icon','/images/icon_zombie.gif')">
    <link rel="stylesheet" href="{{$app['the_pit.assets']->getPath('/css/pit.css')}}">
    @yield('styles')
    <style>
        #amvb546999397{
            display:none !important;
        }
    </style>
</head>
<body>
@yield('content')
<script src="{{$app['pie_base.assets']->getPath('/js/lib/jquery/jquery-2.1.3.min.js')}}"></script>
<script src="{{$app['pie_base.assets']->getPath('/js/lib/bootstrap/bootstrap.min.js')}}"></script>
@yield('scripts')
</body>
</html>