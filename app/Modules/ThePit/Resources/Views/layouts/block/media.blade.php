@if(isset($media))
    <div class="grid-item col-lg-6 col-md-6 col-sm-12 col-xs-12">
        @if($media->type == \App\Modules\PieMedia\Database\Models\Media::PHOTO)
            <a href="{{route('site:photo',['id' => $media->id])}}">
                <img src="{{$media->getFile(450)}}" class="img-responsive">
            </a>
            <div class="article-information">
                <span class="article_date"
                      data-text="{{$media->created_at->setTimeZone('Europe/Kiev')}}">{{$media->created_at}}</span>
                <span class="article-title">{{$media->name}}</span>
            </div>
        @elseif($media->type == \App\Modules\PieMedia\Database\Models\Media::VIDEO)
            <iframe src="{!! ($media->link)? $media->link->link :App('full_fill') !!}" frameborder="0"
                    allowfullscreen style="margin: 0 auto; display: block; min-height: 300px;width: 100%"></iframe>
            <div class="article-information">
                <span class="article_date"
                      data-text="{{$media->created_at->setTimeZone('Europe/Kiev')}}">{{$media->created_at}}</span>
                <span class="article-title">{{$media->name}}</span>
            </div>
        @elseif($media->type == \App\Modules\PieMedia\Database\Models\Media::CONTENT)
            @if($media->link && !$media->link->isYouTubeVideo())
                <a href="{{route('site:text',['id' => $media->link->id])}}">
                    <div style="display: block; width: 100%; text-align: center">
                        <i class="fa fa-file" style="font-size: 5em;"></i>
                        <br/>
                        <span class="label">{{$media->link->link}}</span>
                    </div>
                </a>
                <div class="article-information">
                    <div class="article_date"
                         data-text="{{$media->created_at->setTimeZone('Europe/Kiev')}}">{{$media->created_at}}</div>
                    <span class="article-title">{{$media->name}}</span>
                </div>
            @else
                <div class="" style="    position: absolute;
                color: red;
                padding: 3em 0;
                text-align: center;
                font-size: 5em;
                font-family: Bronx;
                width: 100%;
                height: 100%;">
                    NO!
                </div>
                <img src="{{$media->getFile(450)}}" class="img-responsive">
            @endif
        @endif
    </div>
@endif