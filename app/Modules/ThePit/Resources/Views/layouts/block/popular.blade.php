<div class="grid-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <h4 class="popular_title">{{trans('the_pit::main.news_popular')}}</h4>
    <ul class="list-group">
        @foreach($popular as $model)
            <li class="list-group-item">
                <div class="popular-header-group">
                    @if($model->category)
                        <a href="{{route('site:news',['category'=>$model->category->id])}}"
                           style="background-color: {{$model->category->color}};" class="popular-category tag">
                            {{$model->category->name}}
                        </a>
                    @endif
                    <span class="popular-views">
                        <i class="fa fa-eye"></i> {{$model->views}}&nbsp;
                        <i class="fa fa-comments-o"></i> {{$model->comments->count()}}
                    </span>
                </div>
                <a href="{{route('site:news:article',['id'=>$model->id])}}">
                    <div class="popular-body">
                        <?php $translate = $model->getTranslateArticle($lang); ?>
                        <span class="model_name">{{$translate['name']}}</span>
                        <span class="model_info">{{$translate['info']}}</span>
                    </div>
                </a>
                <div class="popular-footer">
                    <span class="article_date"
                          data-text="{{$model->created_at->setTimeZone('Europe/Kiev')}}">{{$model->created_at}}</span>
                </div>
            </li>
        @endforeach
    </ul>
</div>