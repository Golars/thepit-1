@if(isset($article))
    <div class="grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
        <a href="{{route('site:news:article',['id'=>$article->id])}}">
            <img src="{{$article->getCover(350)}}" class="img-responsive">
            <div class="article-information">
                <span class="article_date"
                      data-text="{{$article->created_at->setTimeZone('Europe/Kiev')}}">{{$article->created_at}}</span>
                <span class="article-title">{{$article->title}}</span>
                <span class="article-info">{{$article->info}}</span>
                {{--<span class="views">--}}
                    {{--<i class="fa fa-eye"></i> {{$article->views}}--}}
                {{--</span>--}}
            </div>
        </a>
    </div>
@endif