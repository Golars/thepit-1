@if(isset($menu) && $menu)
    @foreach($menu as $item)
        <li>
            <a class="page-scroll" href="{{route('site:slug',$item->slug)}}">{{$item->title}}</a>
        </li>
    @endforeach
@else
    <li>
        <a class="page-scroll" href="{{route('site:info')}}">{{trans('the_pit::main.menu_info')}}</a>
    </li>
    <li>
        <a class="page-scroll" href="{{route('site:contacts')}}">{{trans('the_pit::main.menu_contacts')}}</a>
    </li>
@endif
<li>
    <a class="page-scroll" href="{{route('site:news')}}">{{trans('the_pit::main.menu_news')}}</a>
</li>
<li>
    <a class="page-scroll" href="{{route('site:media')}}">{{trans('the_pit::main.menu_media')}}</a>
</li>
@if(Request::user())
    @if(Request::user()->canAdministrate())
        <li>
            <a class="page-scroll btn btn-fucker btn-fucker-a" href="{{route('admin:index')}}"><i class="fa fa-cog"></i> Administration</a>
        </li>
    @endif
@endif
