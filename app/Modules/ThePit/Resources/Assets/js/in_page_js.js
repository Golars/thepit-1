/**
 * Created by Nerdjin on 24.08.2016.
 */

var page = 0;

var comment_block = function (data) {
    return '<div class="col-xs-10 col-xs-offset-1 panel panel-comment">' +
        '<div class="col-md-2 col-sm-4 col-xs-4 text-center user-comment-block">' +
        '<img src="' + data.user.avatar + '" class="img-circle"' +
        ' style="display: block; margin: 0 auto">' +
        '</div>' +
        '<div class="col-md-10 col-sm-8 col-xs-8">' +
        '<b style="display: block">' + data.user.name + '</b>' +
        data.text +
        '</div>' +
        '<div class="comment-date">' +
        '<span data-text="' + data.created_at.date + '"' +
        ' class="article_date tag tag-a">' + data.created_at.date + '</span>' +
        '</div>' +
        '</div>';
}

function clickMore(element) {
    element.on('click', function () {
        page = page + 1;
        loadComments(page);
    });
}

function clickComment(element){
    element.on('click',function(){
        var $name = element.find('b').text();
        var $textarea = $('#comment').find('textarea');
        if($textarea != 'undefined'){
            return false;
        }
        var $text = $textarea.val();
        if($text.indexOf($name) == -1){
            $textarea.val($name + ', '+ $text);
        } else {
            $textarea.val($text);
        }
        $textarea.focus()
    });
}

//Функция отображения PopUp
function PopUpShow() {
    $("#loading").fadeIn(500);
}
//Функция скрытия PopUp
function PopUpHide() {
    $("#loading").fadeOut(500);
}

$(document).ready(function () {
    loadComments(page);
    PopUpHide();
    setInterval(function () {
        momentoForDates()
    }, 10000);
    $('#comment').on('submit', function () {
        PopUpShow();
        var $form = $(this);
        $.ajax({
            url: $form.attr('action'),
            method: 'post',
            data: $form.serializeArray(),
            dataType: 'json',
            success: function (response) {
                $form.find('textarea').val('');
                $('#comment_block').prepend(comment_block(response.data));
                clickComment($('.panel-comment').first());
                momentoForDates();
                PopUpHide();
            },
            error: function (message) {
                PopUpHide();
                console.log(message);
            }
        });
        return false;
    });
    
});
