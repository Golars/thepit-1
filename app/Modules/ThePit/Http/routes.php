<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(['prefix' => '/', 'middleware' => 'web'], function () {
    Route::group(['prefix' => '/', 'middleware' => 'webAdmin'], function () {
        Route::group(['as' => 'site:', 'middleware' => ['ware']], function () {
            Route::get('/', ['as' => 'index', 'uses' => 'ThePitController@indexAction']);
            Route::get('/info', ['as' => 'info', 'uses' => 'ThePitController@infoAction']);
            Route::get('/contacts', ['as' => 'contacts', 'uses' => 'ThePitController@contactsAction']);
            Route::any('/news', ['as' => 'news', 'uses' => 'ThePitController@newsAction']);
            Route::get('/news/category_{category}', ['as' => 'news:category', 'uses' => 'ThePitController@newsAction']);
            Route::get('/news/{id}', ['as' => 'news:article', 'uses' => 'ThePitController@oneArticleAction']);
            Route::get('/media', ['as' => 'media', 'uses' => 'ThePitController@mediaAction']);
            Route::get('/auth/{provider}', [
                    'as' => 'socialite.auth',
                    'uses' => 'ThePitController@redirect'
                ]
            );
            Route::get('/logout', ['as' => 'socialite:logout', 'uses' => 'ThePitController@logoutAction']);
            Route::post('/login', ['as' => 'socialite:login', 'uses' => 'ThePitController@loginAction']);
            Route::get('/auth/{provider}/callback', ['uses' => 'ThePitController@authAction']);
            Route::post('/add/{id}/comment', ['as' => 'api:add:comment', 'uses' => 'ThePitController@addCommentAction']);
            Route::get('/api/comments/{id}/load', ['as' => 'api:load:comments', 'uses' => 'ThePitController@loadCommentAction']);
            Route::get('/{slug}', ['as' => 'slug', 'uses' => 'ThePitController@slugAction']);
        });
    });
});