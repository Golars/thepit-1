<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(['as' => 'admin', 'prefix' => 'admin', 'middleware' => ['webAdmin']], function() {
	Route::group(['middleware' => 'JournalistAuth'], function(){
		Route::group(['prefix' => 'translate'], function(){
			$asAction = ':translate';
			Route::get('/', ['as' => $asAction . ':index', 'uses' => 'Admin\ArticlesTranslatorController@index']);
			Route::get('/add/{id}', ['as' => $asAction.':add:translate', 'uses' => 'Admin\ArticlesTranslatorController@addTranslate']);
			Route::post('/add', ['as' => $asAction.':add', 'uses' => 'Admin\ArticlesTranslatorController@add']);
			Route::any('/{id}/edit', ['as' => $asAction.':edit', 'uses' => 'Admin\ArticlesTranslatorController@edit']);
		});
	});
});