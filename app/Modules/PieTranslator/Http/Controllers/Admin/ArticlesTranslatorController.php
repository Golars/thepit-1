<?php
namespace App\Modules\PieTranslator\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class ArticlesTranslatorController extends Controller
{
    protected $prefix = 'admin:translate';
    protected $moduleName = 'pie_translator';
    protected $serviceName = 'App\Modules\PieTranslator\Http\Services\ArticleTranslator';

    public function index()
    {
        return redirect(route('admin:news:index'));
    }

    protected function prepareData($model = null)
    {
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
            'lang' => App::make('App\Modules\PieTranslator\Http\Services\Languages')->prepareSelect(),
        ];
    }

    public function add(Request $request)
    {
        $this->setRules([
            'trans_name' => 'required|min:2',
            'trans_info' => 'required|min:3',
            'trans_text' => 'required|min:5',
            'article_id' => 'required',
            'language' => 'required',
            'status' => ''
        ]);
        if($this->service->getModel()
            ->where('article_id',$request->input('article_id'))
            ->where('language',$request->input('language'))
            ->first()){
            return $this->sendWithErrors(['This language already added']);
        }
        return parent::add($request);
    }

    public function edit(Request $request, $id)
    {
        $this->setRules([
            'id' => 'required',
            'trans_name' => 'required|min:2',
            'trans_info' => 'required|min:3',
            'trans_text' => 'required|min:5',
            'article_id' => 'required',
            'language' => 'required',
            'status' => ''
        ]);
        return parent::edit($request, $id);
    }

    public function addTranslate(Request $request, $id)
    {
        $article = App::make('App\Modules\PieArticle\Http\Services\Article')->getOne($id);
        return view($this->getViewRoute('edit'),['article' => $article] +  $this->prepareData());
    }
}