<?php
namespace App\Modules\PieTranslator\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PieTranslatorDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('App\Modules\PieTranslator\Database\Seeds\FoobarTableSeeder');
	}

}
