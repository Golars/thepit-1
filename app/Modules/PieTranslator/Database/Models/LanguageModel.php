<?php
namespace App\Modules\PieTranslator\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;

class LanguageModel extends Base
{
    public $timestamps = false;
    protected $table = 'languages';

    protected $fillable = array(
        'name',
    );
}