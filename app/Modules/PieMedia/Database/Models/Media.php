<?php
namespace App\Modules\PieMedia\Database\Models;

use App\Modules\PieBase\Database\Models\Base;

/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 24.07.2016
 * Time: 10:04
 */
class Media extends Base
{
    public $timestamps = true;
    protected $table = 'media';

    protected $fillable = array(
        'name',
        'file_id',
        'article_id',
        'status',
        'type'
    );

    CONST PHOTO = 1;
    CONST VIDEO = 2;
    CONST CONTENT = 3;

    public function file()
    {
        return $this->belongsTo('\App\Modules\PieMedia\Database\Models\File', 'file_id');
    }

    public function getFile($w = null)
    {
        return ($this->file) ? $this->file->getCover($w) : App('full_fill');
    }

    public function article()
    {
        return $this->belongsTo('\App\Modules\PieArticle\Database\Models\Articles', 'article_id');
    }

    public function setFileIdAttribute($value)
    {
        if (isset($value)) {
            $this->attributes['file_id'] = $value;
        }
    }

    public function files()
    {
        return $this->belongsToMany('\App\Modules\PieMedia\Database\Models\File', 'files_to_media');
    }
    
    public function link()
    {
        return $this->hasOne('\App\Modules\PieMedia\Database\Models\LinksToMedia');
    }

}