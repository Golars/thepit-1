<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 24.07.2016
 * Time: 22:57
 */

namespace App\Modules\PieMedia\Database\Models;


use App\Modules\PieBase\Database\Models\Base;

class FilesToMedia extends Base
{
    public $timestamps = true;
    protected $table = 'files_to_media';

    protected $fillable = array(
        'status',
        'file_id',
        'media_id',
        'user_id',
    );

    public function file()
    {
        return $this->hasMany('\App\Modules\PieMedia\Database\Models\File', 'id','file_id');
    }
}