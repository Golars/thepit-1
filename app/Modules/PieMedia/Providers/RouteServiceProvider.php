<?php
namespace App\Modules\PieMedia\Providers;

use App\Modules\PieBase\Providers\Module;
use Caffeinated\Modules\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
	/**
	 * This namespace is applied to the controller routes in your module's routes file.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'App\Modules\PieMedia\Http\Controllers';
	protected $moduleName = 'pie_media';
	protected $assetsPath = 'assets/media/';

	/**
	 * Define your module's route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router $router
	 * @return void
	 */
	public function boot(Router $router)
	{
		parent::boot($router);
		$this->initAssets();
		$router->middleware('JournalistAuth',  \App\Modules\PieMedia\Http\Middleware\JournalistAuth::class);
	}

	protected function initAssets(){
		$this->publishes([
			__DIR__.'/../Resources/Assets' => public_path($this->assetsPath),
		], $this->moduleName);

		$this->app->bind('pie_media.assets', function() {
			return new Module($this->assetsPath);
		});

	}

	/**
	 * Define the routes for the module.
	 *
	 * @param  \Illuminate\Routing\Router $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$router->group(['namespace' => $this->namespace], function($router)
		{
			require (config('modules.path').'/PieMedia/Http/routes.php');
		});
	}
}
