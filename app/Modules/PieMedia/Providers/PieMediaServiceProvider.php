<?php
namespace App\Modules\PieMedia\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieMediaServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieMedia module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieMedia\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieMedia module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_media', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pie_media', base_path('resources/views/vendor/pie_media'));
		View::addNamespace('pie_media', realpath(__DIR__.'/../Resources/Views'));
	}
}
