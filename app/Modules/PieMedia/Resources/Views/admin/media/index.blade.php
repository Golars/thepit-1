@extends('pie_base::admin.layouts.all')

@section('page_title') {{trans('pie_base::main.menu_media')}} @endsection

@section('tHead')
    <th>{{trans('pie_base::main.table_name')}}</th>
    <th>{{trans('pie_article::main.table_cover')}}</th>
    <th>{{trans('pie_base::main.table_type')}}</th>
    <th>{{trans('pie_base::main.table_files')}}</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->role_id == $model::STATUS_DELETED) ? "warning" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->name}}
            </td>
            <td>
                <img src="{{$model->getFile(100)}}" width="75" class="img-responsive">
            </td>
            <td>
                @if($model->type == \App\Modules\PieMedia\Database\Models\Media::PHOTO)
                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                @elseif($model->type == \App\Modules\PieMedia\Database\Models\Media::VIDEO)
                    <i class="fa fa-film" aria-hidden="true"></i>
                @else
                    <i class="fa fa-file" aria-hidden="true"></i>
                @endif
            </td>
            <td>
                {{$model->files->count()}}
            </td>
            <td class="last">
                <a class="btn {{($model->status)?"btn-warning":"btn-info"}}"
                   href="{{route('admin:media:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                <a class="btn btn-success" href="{{route('admin:media:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection