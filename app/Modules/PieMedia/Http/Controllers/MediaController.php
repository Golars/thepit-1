<?php
namespace App\Modules\PieMedia\Http\Controllers;

use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use App\Modules\PieMedia\Database\Models\LinksToMedia;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 24.07.2016
 * Time: 10:46
 */
class MediaController extends Controller
{
    protected $prefix = 'site:media';
    protected $moduleName = 'pie_media';
    protected $serviceName = 'App\Modules\PieMedia\Http\Services\Media';

    public function loadContent(Request $request, $id)
    {
        $link = LinksToMedia::query()->find($id);
        if (!$link) {
            abort(404);
        }
        return view($this->getViewRoute('content'), [
            'link' => "http://" . $_SERVER['HTTP_HOST'] . $link->link
        ]);
    }

    public function loadPhoto(Request $request, $id)
    {
        $media = $this->service->getOne($id);
        if (!$media) {
            abort(404);
        }
        return view($this->getViewRoute('photo'), [
            'model' => $media,
            'lang' => 'en'
        ]);
    }
}