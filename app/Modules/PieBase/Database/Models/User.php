<?php
namespace App\Modules\PieBase\Database\Models;

use App;
use Illuminate\Support\Facades\Hash;

class User extends Base
{
    protected $path = 'user_files/';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    const STATUS_DELETED = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('email', 'password', "first_name", 'last_name', 'file_cover_id', 'role_id', 'login', 'status');

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function token()
    {
        return $this->hasOne('\App\Modules\PieBase\Database\Models\Token', 'user_id');
    }

    /**
     * Set the user's Password.
     *
     * @param  string $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = $this->_enPass($value);
        }
    }

    public function chkPassword($password)
    {
        return Hash::check($password, $this->password);
    }

    private function _enPass($pass)
    {
        return Hash::make($pass);
    }

    public function createToken(User $user)
    {
        $this->token = new Token();
        $this->token->token = md5($user->id . time());
        $this->token->user_id = $user->id;
        $this->token->save();
    }

    public function getCover($w = null)
    {
        return (isset($this->cover->url)) ? $this->cover->getCover($w) : App::make('pie_base.assets')->getPath('images/user.png');
    }

    public function cover()
    {
        return $this->belongsTo('\App\Modules\PieBase\Database\Models\Cover', 'file_cover_id');
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getRole()
    {
        if ($this->role_id == self::STATUS_DELETED || $this->role->status != Base::STATUS_ACTIVE) {
            $this->role = (new Role())->setDefault();
        }
        return $this->role;
    }

    public function setDefault()
    {
        $this->id = 0;
        $this->role_id = 0;
        $this->first_name = 'User';
        $this->last_name = 'Deleted';
        return $this;
    }

    public function setFileCoverIdAttribute($value)
    {
        if (isset($value)) {
            $this->attributes['file_cover_id'] = $value;
        }

    }

    public function isAdmin()
    {
        return ($this->role && $this->role->id == Role::ADMIN && $this->role->isAdminRules()) ? true : false;
    }

    public function isModerator()
    {
        return ($this->role && $this->role->id == Role::ADMIN && $this->role->isAdminRules()) ? true : false;
    }
    
    public function isJournalist()
    {
        return ($this->role && $this->role->id == Role::JOURNALIST && $this->role->isJournalistRules()) ? true : false;
    }

    public function getAvatar($w = null)
    {
        return (isset($this->cover->url)) ? $this->cover->getAvatar($w) : App::make('pie_base.assets')->getPath('images/user.png');
    }

    public function articles()
    {
        return $this->hasMany('\App\Modules\PieArticle\Database\Models\Articles', 'user_id','id');
    }

    public function comments()
    {
        return $this->hasMany('\App\Modules\PieComments\Database\Models\Comments', 'user_id','id');
    }

    public function canAdministrate()
    {
        return $this->isAdmin() || $this->isJournalist() || $this->isModerator();
    }
}