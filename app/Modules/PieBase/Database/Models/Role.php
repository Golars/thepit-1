<?php
namespace App\Modules\PieBase\Database\Models;

/**
 * @property $is_admin
 * @property $name
 * @property $status
 */
class Role extends Base
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('name', 'is_admin', 'status');

    public $timestamps = false;

    CONST JOURNALIST = 4;
    CONST ADMIN = 3;
    CONST MODERATOR = 2;
    CONST USER = 1;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('\App\Modules\PieBase\Database\Models\User');
    }

    public function setDefault()
    {
        $this->id = 0;
        $this->is_admin = 0;
        $this->name = 'Deleted';
        return $this;
    }

    public function setIsAdminAttribute($value)
    {
        $this->attributes['is_admin'] = (isset($value));
    }

    public function setIsJournalistAttribute($value)
    {
        $this->attributes['is_journalist'] = (isset($value));
    }

    public function isAdminRules()
    {
        return ($this->is_admin) ? true : false;
    }

    public function isJournalistRules()
    {
        return ($this->is_journalist) ? true : false;
    }
}
