<?php
namespace App\Modules\PieBase\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieBaseServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieBase module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieBase\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieBase module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_base', realpath(__DIR__.'/../Resources/Lang'));
		View::addNamespace('pie_base', base_path('resources/views/vendor/pie_base'));
		View::addNamespace('pie_base', realpath(__DIR__.'/../Resources/Views'));
	}
}
