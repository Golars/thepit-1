<?php

namespace App\Modules\PieBase\Http\Controllers\Admin;

use App\Modules\PieArticle\Http\Services\Article;
use App\Modules\PieBase\Http\Services\User;

class MainController extends Controller{

    public function index(){
        $articles = new Article();
        $users = new User();
        $articles->getDiscussed();
        return view($this->getViewRoute(),[
            'popular' => $popular = $articles->getPopular($this->config->findBySlug('admin_padding')),
            'discussed' => $articles->getDiscussed($this->config->findBySlug('admin_padding')),
            'users' => $users->getLastUsers($this->config->findBySlug('admin_padding'))
        ]);
    }
}
