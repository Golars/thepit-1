<?php
namespace App\Modules\PieBase\Http\Services;

class User extends Base
{
    protected $modelName = 'App\Modules\PieBase\Database\Models\User';

    public function auth($data)
    {
        $user = $this->getModel()->newQuery()
            ->where('login', $data['login'])
            ->orWhere('email', $data['login'])
            ->with('token')
            ->with('role')
            ->first();
        if (!$user || !$user->chkPassword($data['password'])) {
            return $this->addError('Email/Login or password is invalid');
        }

        if (!$user->token) {
            $user->createToken($user);
        }
        $this->setModel($user);
    }

    public function authByToken($token)
    {
        if (!$token) {
            return;
        }
        $user = $this->getModel()->newQuery()
            ->whereHas('token', function ($query) use ($token) {
                $query->where('token', $token);
            })
            ->with('role')
            ->first();
        if (!isset($user->id)) {
            return;
        }
        $this->setModel($user);
        return $user;
    }

    public function prepareSelect()
    {
        $users = [];
        foreach ($this->getAll() as $user) {
            $users[$user->id] = $user->getFullName();
        }
        return $users;
    }

    public function getLastUsers($take = 5)
    {
        return $this->getModel()->query()->orderBy('created_at', 'desc')->take($take)->get();
    }
}