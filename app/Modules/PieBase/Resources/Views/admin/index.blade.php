@extends('pie_base::admin.layouts.default')
@section('head')
    <style>
        .admin-box {
            display: table;
            width: 100%;
            padding: 5px;
            border: solid 1px #2b2a28;
            border-radius: 3px;
            box-shadow: 0 0 1px #34495e;
            color: #fff;
            margin: 10px 0 5px 0;
            min-height: 50px;
            position: relative;
        }

        .admin-box:hover {
            box-shadow: 0 0 5px #34495e;
        }

        .admin-title {
            font-family: 'Courier New', Monospace;
        }

        .admin-body {
            font-size: .7em;
            display: table-footer-group;
        }

        .wet-asphalt {
            background-color: #34495e;
        }

        .wisteria {
            background-color: #8e44ad;
        }

        .belize-hole {
            background-color: #2980b9;
        }

        .peter-river {
            background-color: #3498db;
            color: #fff;
        }

        .carrot {
            background-color: #e67e22;
        }

        .emerald {
            background-color: #2ecc71;
        }

        .most {
            font-size: 1.1em;
            font-family: 'Courier New', Monospace;
            border-bottom: solid 1px;
            padding: 5px;
        }

        .admin-icon {
            position: absolute;
            right: .5em;
            top: .3em;
            font-size: 1.5em;
            display: none;
        }

        .admin-icon > a {
            text-decoration: none;
            color: #f0f0f0;
        }

        .admin-box:hover > .admin-icon {
            display: block;
        }
    </style>

@endsection
{{-- Content --}}
@section('content')
    <h1><img src="{{$app['logo']}}" width="75px"> Pie Admin Panel</h1>
    <div class="clear-fix"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                <b class="most"><i class="fa fa-star"></i> Most popular articles</b>
                @foreach($popular as $model)
                    <div class="admin-box wet-asphalt">
                        <div class="admin-title">
                            <i class="fa fa-newspaper-o"></i> {{$model->name}}
                        </div>
                        <div class="admin-body">
                            {{$model->info}}
                            <br/>
                            <i class="fa fa-eye"></i> {{$model->views}}
                            <i class="fa fa-comments-o"></i> {{$model->comments->count()}}
                        </div>
                        <div class="admin-icon">
                            <a href="{{route('admin:news:edit',['id'=>$model->id])}}">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>
                            <a href="{{route('site:news:article',['id'=>$model->id])}}" target="_blank">
                                <i class="fa fa-arrow-circle-o-right"></i>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-xs-4">
                <b class="most"><i class="fa fa-comments"></i> Most discussed articles</b>
                @foreach($discussed as $model)
                    <div class="admin-box wisteria">
                        <div class="admin-title">
                            <i class="fa fa-comment"></i> {{$model->name}}
                        </div>
                        <div class="admin-body">
                            {{$model->info}}
                            <br/>
                            <i class="fa fa-eye"></i> {{$model->views}}
                            <i class="fa fa-comments-o"></i> {{$model->comments->count()}}
                        </div>
                        <div class="admin-icon">
                            <a href="{{route('admin:news:edit',['id'=>$model->id])}}">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>
                            <a href="{{route('site:news:article',['id'=>$model->id])}}" target="_blank">
                                <i class="fa fa-arrow-circle-o-right"></i>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-xs-4">
                <b class="most"><i class="fa fa-users"></i> New Users</b>
                @foreach($users as $model)
                    <div class="admin-box emerald">
                        <div class="admin-title">
                            <i class="fa fa-user"></i> {{$model->getFullName()}}
                        </div>
                        <div class="admin-body">
                            {{$model->role->name}}<br/>
                            <i class="fa fa-newspaper-o"></i> {{$model->articles->count()}}
                            <i class="fa fa-comments-o"></i> {{$model->comments->count()}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@stop
