@extends('pie_base::admin.layouts.default')

{{-- Content --}}
@section('content')

    <div class="row">
        <div class="x_title">
            <h2>
                <small>administrate</small> @yield('page_title')
            </h2>
            @include('pie_base::admin.layouts.block.top_menu')
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <table id="collection" class="table table-striped responsive-utilities jambo_table">
                        <thead>
                        <tr class="headings">
                            <th>
                                <input type="checkbox" class="tableflat">
                            </th>
                            @yield('tHead')
                            <th class=" no-link last">
                                <span class="nobr">{{trans('pie_base::main.table_acts')}}</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            @yield('tBody')
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <!-- Datatables -->
    <script src="{{$app['pie_base.assets']->getPath('js/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/datatables/tools/js/dataTables.tableTools.js')}}"></script>
    <script>
        $(document).ready(function(){
            PopUpHide();
        });
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        function PopUpShow(){
            $("#popup1").show();
            $("#img1").setAttribute('src','g.jpg')

        }
        function PopUpHide(){
            $("#popup1").hide();
        }

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#collection').dataTable({
                "oLanguage": {
                    "sSearch": "Find in all cols:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
                ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "{{$app['pie_base.assets']->getPath('js/lib/datatables/tools/swf/copy_csv_xls_pdf.swf')}}"
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
@stop