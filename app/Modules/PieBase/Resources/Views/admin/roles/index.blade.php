@extends('pie_base::admin.layouts.all')

@section('page_title', 'Роли <small>управление ролями</small>')

@section('tHead')
    <th>Id</th>
    <th>{{trans('pie_base::main.table_role')}}</th>
    <th>Admin</th>
    <th>{{trans('pie_base::main.table_status')}}</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->id}}</td>
            <td>{{$model->name}}</td>
            <td><h2 class="label label-info"><?php echo ($model->is_admin) ? '+' : '-'?></h2> </td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn btn-success" href="{{$getRoute('edit', ['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn {{($model->status)?"btn-warning":"btn-info"}}" href="{{$getRoute('active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                @if($model->status != $model::STATUS_ACTIVE)
                    <a class="btn btn-danger" href="{{$getRoute('delete', ['id'=>$model->id])}}">
                        <i class="fa fa-trash"></i>
                    </a>
                @endif
            </td>
        </tr>
    @endforeach
@endsection


