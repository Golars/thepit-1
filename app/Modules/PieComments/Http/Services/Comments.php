<?php
namespace App\Modules\PieComments\Http\Services;

use App;
use App\Modules\PieBase\Http\Services\Base;
use Illuminate\Http\Request;

class Comments extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieComments\Database\Models\Comments';

    protected $orderBy = ['created_at' => 'desc'];
    
    public function oneArticle($id)
    {
        $model = $this->getModel();
        $this->setWhere(['status' => $model::STATUS_ACTIVE]);
        return $this->getOne($id);
    }
}