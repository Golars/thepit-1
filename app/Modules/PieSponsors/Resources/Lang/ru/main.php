<?php
return [
    'title' => 'Спонсоры',
    'sub_title' => 'Список спонсоров',
    'admin_name' => 'Название',
    'admin_link' => 'Ссылка',
    'admin_emblem' => 'Эмблема',
];