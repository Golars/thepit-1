<?php
namespace App\Modules\PieSponsors\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;

/**
 * Class Articles
 * @package App\Modules\PieArticle\Database\Models
 * @property App\Modules\PieTranslator\Database\Models\TranslateArticles $lang
 */
class Sponsor extends Base
{
    public $timestamps = true;
    protected $table = 'sponsors';

    protected $fillable = array(
        'name',
        'link',
        'cover_id',
        'status',
    );

    public function cover()
    {
        return $this->belongsTo(Cover::class, 'cover_id');
    }

    public function getCover($w = null)
    {
        return ($this->cover) ? $this->cover->getCover($w) : App('logo');
    }

    public function setCoverIdAttribute($value)
    {
        if (isset($value)) {
            $this->attributes['cover_id'] = $value;
        }
    }

    public function getLinkAttribute()
    {
        if(stripos($this->attributes['link'],'http://') === 0){
            return $this->attributes['link'];
        }
        return "http://" . $this->attributes['link'];
    }

}