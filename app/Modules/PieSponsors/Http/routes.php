<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => '/', 'middleware' => 'web'], function () {
	Route::group(['prefix' => 'admin', 'middleware' => ['webAdmin']], function () {
		$asPrefix = 'admin';
		Route::group(['middleware' => 'AdminAuthenticate'], function () use ($asPrefix) {
			Route::group(['prefix' => 'sponsors'], function () use ($asPrefix) {
				$adAction = ':sponsors';
				Route::get('/', ['as' => $asPrefix . $adAction . ':index', 'uses' => 'Admin\SponsorsController@index']);
				Route::get('/{id}/delete', ['as' => $asPrefix . $adAction . ':delete', 'uses' => 'Admin\SponsorsController@delete']);
				Route::any('/add', ['as' => $asPrefix . $adAction . ':add', 'uses' => 'Admin\SponsorsController@add']);
				Route::any('/{id}/edit', ['as' => $asPrefix . $adAction . ':edit', 'uses' => 'Admin\SponsorsController@edit']);
				Route::get('/{id}/active', ['as' => $asPrefix . $adAction . ':active', 'uses' => 'Admin\SponsorsController@active']);
			});
		});
	});
});