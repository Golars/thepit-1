<?php
namespace App\Modules\PieSponsors\Http\Services;

use App\Modules\PieBase\Http\Services\Base;

class Sponsor extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieSponsors\Database\Models\Sponsor';
    protected $hasStatus = true;

}