<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['as' => 'admin', 'prefix' => 'admin', 'middleware' => ['webAdmin']], function() {
	Route::group(['middleware' => 'JournalistAuth'], function(){
		Route::group(['prefix' => 'static'], function(){
			$asAction = ':static';
			Route::get('/', ['as' => $asAction . ':index', 'uses' => 'Admin\StaticController@index']);
			Route::any('/add', ['as' => $asAction.':add', 'uses' => 'Admin\StaticController@add']);
			Route::get('/{id}/active', ['as' => $asAction.':active', 'uses' => 'Admin\StaticController@active']);
			Route::any('{id}/delete', ['as' => $asAction.':delete', 'uses' => 'Admin\StaticController@delete']);
			Route::any('/{id}/edit', ['as' => $asAction.':edit', 'uses' => 'Admin\StaticController@edit']);
			Route::get('/translate/index', ['as' => $asAction . ':translate:index', 'uses' => 'Admin\StaticTranslateController@index']);
			Route::get('/translate/add/{id}', ['as' => $asAction.':translate:add', 'uses' => 'Admin\StaticTranslateController@addTranslate']);
			Route::post('/translate/add', ['as' => $asAction.':translate:save', 'uses' => 'Admin\StaticTranslateController@saveTranslate']);
			Route::any('/translate/edit/{id}', ['as' => $asAction.':translate:edit', 'uses' => 'Admin\StaticTranslateController@editTranslate']);
		});
		Route::group(['prefix' => 'api'], function(){
			$asAction = ':api';
			Route::any('/upload', ['as' => $asAction . ':upload', 'uses' => 'Admin\StaticController@upload']);
		});
	});
});

Route::group(['as' => 'static:', 'middleware' => ['webAdmin']], function() {
	Route::get('/page/{slug}', ['as' => 'page', 'uses' => 'StaticController@slugAction']);
});
