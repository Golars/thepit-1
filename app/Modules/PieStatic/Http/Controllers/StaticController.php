<?php

namespace App\Modules\PieStatic\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 21.07.2016
 * Time: 17:28
 */

use App\Modules\PieBase\Http\Controllers\Controller;
use App\Modules\PieStatic\Http\Services\StaticPie;
use Illuminate\Http\Request;

class StaticController extends Controller
{
    protected $prefix = 'static';
    protected $moduleName = 'pie_static';

    public function slugAction(Request $request,$slug)
    {
        $page = new StaticPie();
        $model = $page->getPageBySlug($slug);
        if(!$model){
            abort(404);
        }
        return view($this->getViewRoute('page'),['content' => $model]);
    }
}