<?php
namespace App\Modules\PieStatic\Http\Services;

use App\Modules\PieBase\Http\Services\Base;

class StaticPieTranslate extends Base
{
    protected $model;
    protected $modelName = 'App\Modules\PieStatic\Database\Models\PiePageTranslate';
}