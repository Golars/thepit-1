<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 21.07.2016
 * Time: 15:30
 */

namespace App\Modules\PieStatic\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;

class PiePage extends Base
{
    protected $perPage = 15;

    public $timestamps = true;
    protected $table = 'static_pages';

    protected $fillable = array(
        'title',
        'slug',
        'info',
        'file_id',
        'content',
        'display_image',
        'status',
        'in_menu',
        'position'
    );

    public function cover()
    {
        return $this->belongsTo(Cover::class, 'file_id');
    }

    public function translate()
    {
        return $this->hasMany(PiePageTranslate::class, 'static_page_id', 'id');
    }

    public function getCover($w = null)
    {
        return ($this->cover) ? $this->cover->getCover($w) : App('logo');
    }

    public function setDisplayImageAttribute($value)
    {
        $this->attributes['display_image'] = isset($value);
    }

    public function setInMenuAttribute($value)
    {
        $this->attributes['in_menu'] = isset($value);
    }

    public function scopeMenu($query)
    {
        return $query->where('in_menu', true);
    }

    public function getTranslatePage($language = 1)
    {
        $language = $this->translate->where('language',$language)->first();
        if ($language && $language->status == Base::STATUS_ACTIVE) {
            return (object)[
                'title' => $language->title_translate,
                'info' => $language->info_translate,
                'content' => $language->content_translate,
            ];
        }
        return (object)[
            'title' => $this->title,
            'info' => $this->info,
            'content' => $this->content
        ];
    }
}