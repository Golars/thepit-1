@extends('pie_static::static/blank')
@section('title')
    {{$content->title}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <h1>{{$content->title}}</h1>
            <h3>{{$content->info}}</h3>
            @if($content->display_image)
                <img src="{{$content->getCover(450)}}" class="img-respomsive">
            @endif
            <div>{!! $content->content !!}</div>
        </div>
    </div>
@endsection