@extends('pie_base::admin.layouts.edit')

@section('title_name')
    {{trans('pie_static::main.sub_menu_static')}}
@endsection

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    {!! Form::hidden('file_id', $model->file_id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label('Тайтл') !!}
        {!! Form::text('title', $model->title, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Slug') !!}
        {!! Form::text('slug', $model->slug, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Информация') !!}
        {!! Form::text('info', $model->info, ['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Обложка') !!}
        <div class="addCover" id="addICover">
            <span class="addCoverPhoto"><div class="middle"><i class="glyphicon glyphicon-plus"></i> Выбрать обложку
                </div></span>
            {!! Form::file('cover',['class'=>'hidden','id'=>'selectCover']) !!}
            <img src="{{$model->getCover()}}" width="250" class="img-responsive">
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('Отображать изображение') !!}
        {!! Form::checkbox('display_image',true,$model->display_image) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Отображать в меню') !!}
        {!! Form::checkbox('in_menu',true,$model->in_menu) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Позиция в меню') !!}
        {!! Form::number('position',$model->position ,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Контент') !!}
        {!! Form::textarea('content', $model->content, ['class'=>'form-control','id'=>'edit']) !!}
    </div>
@endsection

@section('scripts')
    @yield('add_script')
    @yield('media_script')
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_editor.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/froala_style.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/css/themes/dark.min.css')}}" rel="stylesheet"
          type="text/css">
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/froala_editor.min.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/froala_editor_ie8.min.js')}}"></script>
    <![endif]-->
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/tables.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/lists.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/colors.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/media_manager.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/font_family.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/font_size.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/block_styles.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/video.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/block_styles.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/fullscreen.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/entities.min.js')}}"></script>
    <script src="{{$app['pie_base.assets']->getPath('js/lib/froala_editor/plugins/fullscreen.min.js')}}"></script>
    <script>
        $(function () {
            $('#edit').editable({
                inlineMode: false,
                theme: 'white',
                height: '300',
                language: 'ru',
                fullPage: true
            });
            $('.froala-box div').last().remove();
        });

    </script>
@endsection