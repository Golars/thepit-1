@extends('pie_base::admin.layouts.all')

@section('page_title') Media @endsection

@section('tHead')
    <th>{{trans('pie_base::main.table_name')}}</th>
    <th>{{trans('pie_base::main.table_value')}}</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->role_id == $model::STATUS_DELETED) ? "warning" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>
                {{$model->name}}
            </td>
            <td>
                {{$model->value}}
            </td>
            <td class="last">
                <a class="btn btn-success" href="{{route('admin:config:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection