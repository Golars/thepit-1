@extends('pie_base::admin.layouts.edit')

@section('title_name', 'Галереи')

@section('form_body')
    {!! Form::hidden('id', $model->id, ['class'=>'form-control']) !!}
    <div class="form-group">
        {!! Form::label('Название') !!}
        {!! Form::text('name', $model->name, ['class'=>'form-control','readonly']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('Значение') !!}
        {!! Form::text('value', $model->value, ['class'=>'form-control']) !!}
    </div>
@endsection

@section('scripts')
@endsection