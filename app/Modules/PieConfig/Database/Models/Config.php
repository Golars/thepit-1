<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 04.09.2016
 * Time: 20:54
 */

namespace App\Modules\PieConfig\Database\Models;

use App\Modules\PieBase\Database\Models\Base;

class Config extends Base
{
    public $timestamps = true;
    protected $table = 'pie_config';

    protected $primaryKey = 'id';

    protected $fillable = array(
        'id',
        'slug',
        'value',
        'name',
    );

    public static function create(array $attributes = [])
    {
        $attributes['slug'] = Base::transliterate(str_replace(' ','_',$attributes['name']));
        return parent::create($attributes);
    }


}