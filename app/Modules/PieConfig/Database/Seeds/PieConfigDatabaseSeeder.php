<?php
namespace App\Modules\PieConfig\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Modules\PieConfig\Database\Models\Config;
use Illuminate\Support\Facades\DB;

class PieConfigDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		DB::table('pie_config')->delete();
		Config::create([
			'name' => 'Paginate count',
			'value' => '12'
		]);
		Config::create([
			'name' => 'Actual news',
			'value' => '6'
		]);
		Config::create([
			'name'=>'Comments count',
			'value'=>'4'
		]);
		Config::create([
			'name'=>'Paginate media',
			'value'=>'6'
		]);
		Config::create([
			'name'=>'Admin padding',
			'value'=>'9'
		]);
	}

}
