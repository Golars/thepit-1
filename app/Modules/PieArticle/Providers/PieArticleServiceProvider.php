<?php
namespace App\Modules\PieArticle\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PieArticleServiceProvider extends ServiceProvider
{
	/**
	 * Register the PieArticle module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\PieArticle\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the PieArticle module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('pie_article', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('pie_article', base_path('resources/views/vendor/pie_article'));
		View::addNamespace('pie_article', realpath(__DIR__.'/../Resources/Views'));
	}
}
