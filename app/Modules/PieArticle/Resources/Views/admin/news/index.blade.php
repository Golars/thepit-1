@extends('pie_base::admin.layouts.all')

@section('page_title') {{trans('pie_article::main.sub_menu_news')}} @endsection

@section('tHead')
    <th>{{trans('pie_base::main.table_name')}}</th>
    <th>{{trans('pie_article::main.table_info')}}</th>
    <th>{{trans('pie_article::main.table_cover')}}</th>
    <th>{{trans('pie_article::main.table_owner')}}</th>
    <th>{{trans('pie_article::main.table_category')}}</th>
    <th>{{trans('pie_base::main.table_status')}}</th>
@endsection

@section('tBody')
    @foreach($collection as $model)
        <tr class="even pointer {{($model->role_id == $model::STATUS_DELETED) ? "warning" : ""}}">
            <td class="a-center ">
                <input name="{{$model->id}}" type="checkbox" class="tableflat">
            </td>
            <td>{{$model->name}}

            </td>
            <td>{{$model->info}}</td>
            <td>
                <img src="{{$model->getCover()}}" width="75" class="img-responsive">
            </td>
            <td>{{$model->getUser()->getFullName()}}</td>
            <td>
                @if(isset($model->getCategory()->name))
                    <span class="label" style="background-color: {{$model->getCategory()->color}};">
                        {{$model->getCategory()->name}}
                    </span>
                @endif
            </td>
            <td>
                <span class=" {{$model->getStateClass()}}">
                    {{($model->getStateName())}}
                </span>
            </td>
            <td class="last">
                <a class="btn {{($model->status)?"btn-warning":"btn-info"}}"
                   href="{{route('admin:news:active',['id'=>$model->id])}}">
                    <i class="fa {{($model->status)?"fa-eye-slash":"fa-eye"}}"></i>
                </a>
                @if($model->lang->first())
                    @foreach($model->lang as $lang)
                        <a class="btn {{($lang->status == 1)?"btn-info":"btn-danger"}}" href="{{route('admin:translate:edit',['id'=>$lang->id])}}">
                            <i class="fa fa-language"></i> {{$lang->lang->name}}
                        </a>
                    @endforeach
                @endif
                @if($model->lang->count() < 3)
                    <a class="btn btn-info" href="{{route('admin:translate:add:translate',['id'=>$model->id])}}">
                        <i class="fa fa-plus"></i> <i class="fa fa-language"></i>
                    </a>
                @endif
                <a class="btn btn-success" href="{{route('admin:news:edit',['id'=>$model->id])}}">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a class="btn btn-danger" href="{{route('admin:news:delete',['id'=>$model->id])}}">
                    <i class="fa fa-remove"></i>
                </a>
            </td>
        </tr>
    @endforeach
@endsection