<?php

namespace App\Modules\PieArticle\Http\Services;

use App\Modules\PieBase\Http\Services\Base;

class Category extends Base{
    protected $orderBy = [];
    protected $modelName = 'App\Modules\PieArticle\Database\Models\Category';

    public function prepareSelect(){
        $categories = [];
        $this->setWhere(['status'=>\App\Modules\PieBase\Database\Models\Base::STATUS_ACTIVE]);
        foreach($this->getAll() as $category){
            $categories[$category->id] = $category->name;
        }

        return $categories;
    }

    public function updateIsMain($id){
        $model = $this->getOne($id);

        if ($model){
            $model->is_main = !$model->is_main;
            return $model->save();
        }
    }
}