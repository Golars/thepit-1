<?php
namespace App\Modules\PieArticle\Http\Controllers\Admin;

use App;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class ArticlesController extends Controller {
    protected $prefix = 'admin:news';
    protected $moduleName = 'pie_article';
    protected $serviceName = 'App\Modules\PieArticle\Http\Services\Article';
    const LAST_TIME = 604800;

    protected function prepareData($model = null){
        return [
            'model' => (isset($model->id)) ? $model : $this->service->getModel(),
            'users' => App::make('App\Modules\PieBase\Http\Services\User')->prepareSelect(),
            'categories' => App::make('App\Modules\PieArticle\Http\Services\Category')->prepareSelect(),
        ];
    }

    public function add(Request $request){
        $this->setRules([
            'name'          =>  'required|min:2',
            'info'          =>  'required|min:3',
            'text'          =>  'required|min:5',
            'image_id'      =>  '',
            'category_id'   =>  'required',
            'user_id'       =>  'required',
        ]);
        if($request->method() != 'GET' && isset($request['cover'])) {
            $imageId = $this->saveFile($request);
            $request->merge(array('image_id' => $imageId));
        }
        return parent::add($request);
    }

    public function edit(Request $request, $id){
        $this->setRules([
            'id'            =>  'required',
            'name'          =>  'required|min:2',
            'info'          =>  'required|min:3',
            'text'          =>  'required|min:5',
            'image_id'      =>  '',
            'category_id'   =>  'required',
            'user_id'       =>  'required',
        ]);

        if($request->hasFile('cover')) {
            $imageId = $this->saveFile($request);
            $request->merge(array('image_id' => $imageId));
        }
        return parent::edit($request, $id);
    }

    private function saveFile(Request $request){
        $service = new App\Modules\PieArticle\Http\Services\Image($request->file('cover'));
        $service->saveFile($request->user()->id);
        return $service->getModel()->id;
    }


}