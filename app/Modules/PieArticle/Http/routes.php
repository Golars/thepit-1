<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/article', function() {
	return view('pie_base::info', ['module'=>'Article Module']);
});

Route::group(['as' => 'admin', 'prefix' => 'admin', 'middleware' => ['webAdmin']], function() {
	Route::group(['middleware' => 'JournalistAuth'], function(){
		Route::group(['prefix' => 'news'], function(){
			$asAction = ':news';
			Route::get('/', ['as' => $asAction . ':index', 'uses' => 'Admin\ArticlesController@index']);
			Route::any('/add', ['as' => $asAction.':add', 'uses' => 'Admin\ArticlesController@add']);
			Route::get('/{id}/active', ['as' => $asAction.':active', 'uses' => 'Admin\ArticlesController@active']);
			Route::any('{id}/delete', ['as' => $asAction.':delete', 'uses' => 'Admin\ArticlesController@delete']);
			Route::any('/{id}/edit', ['as' => $asAction.':edit', 'uses' => 'Admin\ArticlesController@edit']);
		});
		Route::group(['prefix' => 'categories'], function(){
			$asAction = ':categories';
			Route::get('/', ['as' => $asAction . ':index', 'uses' => 'Admin\CategoriesController@index']);
			Route::any('/{id}/edit', ['as' => $asAction .':edit', 'uses' => 'Admin\CategoriesController@edit']);
			Route::any('/add', ['as' => $asAction .':add', 'uses' => 'Admin\CategoriesController@add']);
			Route::get('/{id}/active', ['as' => $asAction.':active', 'uses' => 'Admin\CategoriesController@active']);
			Route::get('/{id}/delete', ['as' => $asAction.':delete', 'uses' => 'Admin\CategoriesController@delete']);
			Route::get('/{id}/update', ['as' => $asAction.':update', 'uses' => 'Admin\CategoriesController@update']);
		});
	});
});