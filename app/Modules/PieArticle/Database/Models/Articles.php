<?php
namespace App\Modules\PieArticle\Database\Models;

use App;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Database\Models\User;

/**
 * Class Articles
 * @package App\Modules\PieArticle\Database\Models
 * @property App\Modules\PieTranslator\Database\Models\TranslateArticles $lang
 */
class Articles extends Base
{
    protected $with = [
        'user',
        'category',
        'cover',
        'comments'
    ];
    public $timestamps = true;
    protected $table = 'articles';

    protected $fillable = array(
        'user_id',
        'name',
        'info',
        'text',
        'image_id',
        'category_id',
        'views',
        'status',
        'media_id',
        'views'
    );

    public function lang()
    {
        return $this->hasMany('App\Modules\PieTranslator\Database\Models\TranslateArticles', 'article_id');
    }

    public function category()
    {
        return $this->belongsTo('\App\Modules\PieArticle\Database\Models\Category', 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function cover()
    {
        return $this->belongsTo('\App\Modules\PieArticle\Database\Models\Cover', 'image_id');
    }

    public function getCategory()
    {
        if (!isset($this->category->id) || $this->category->status != self::STATUS_ACTIVE) {
            $this->category = (new Category())->setDefault();
        }
        return $this->category;
    }

    public function getDate($format = 'j M Y')
    {
        return date($format, strtotime($this->created_at));
    }

    public function updateViews()
    {
        $this->views = ++$this->views;
        self::query()->where('id', $this->id)->update(['views' => $this->views]);
    }

    public function getCover($w = null)
    {
        return ($this->cover) ? $this->cover->getCover($w) : App('logo');
    }

    public function getUser()
    {
        return (isset($this->user)) ? $this->user : App::make('\App\Modules\PieBase\Database\Models\User')->setDefault();
    }

    public function getInfo()
    {
        return [
            'id' => $this->id,
            'cover' => $this->getCover(),
            'name' => $this->name,
            'info' => $this->info,
            'views' => $this->view,
            'category' => $this->category->name,
            'created_at' => $this->getDate()
        ];
    }

    public function setImageIdAttribute($value)
    {
        if (isset($value)) {
            $this->attributes['image_id'] = $value;
        }
    }

    public function comments()
    {
        return $this->hasMany('\App\Modules\PieComments\Database\Models\Comments', 'article_id', 'id');
    }

    public function getTranslateArticle($language = 1)
    {
        $language = $this->lang->where('language', $language)->first();
        if ($language && $language->status == Base::STATUS_ACTIVE) {
            return [
                'name' => $language->trans_name,
                'info' => $language->trans_info,
                'text' => $language->trans_text,
            ];
        }
        return [
            'name' => $this->name,
            'info' => $this->info,
            'text' => $this->text
        ];
    }

    public function getTextAttribute()
    {
        $text = $this->attributes['text'];
        return $this->convertHashtags($text);
    }
    private function convertHashtags($text){
        $regex = "/#+([a-zA-Z0-9_]+)/";
        $str = preg_replace($regex, '<a href="'. route('site:news') .'?filter=$1">$0</a>', $text);
        return($str);
    }
}