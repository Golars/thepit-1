<?php
namespace App\Modules\PieArticle\Database\Models;

use App\Modules\PieBase\Database\Models\File;

class Cover extends File
{
    protected $path = 'articles/';
}
