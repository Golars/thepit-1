<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 20.07.2016
 * Time: 10:50
 */

namespace App\Modules\PieArticle\Database\Models;


use App\Modules\PieBase\Database\Models\Base;

class Category extends Base
{
    protected $table = 'categories';
    public $timestamps = false;
    protected $perPage = 15;
    const COLOR = '000000';
    protected $fillable = array(
        'color',
        'name',
        'status'
    );

    public function setColorAttribute($value){
        $this->attributes['color'] = str_replace('#', '' , $value);
    }

    public function getColorAttribute($value){
        return '#' . $value;
    }

    public function setDefault(){
        $this->id       = 0;
        $this->color    = self::COLOR;
        $this->name     = '-';
        return $this;
    }

    public function getColor(){
        return ($this->color != '#') ? $this->color : $this->setDefault()->color;
    }
}