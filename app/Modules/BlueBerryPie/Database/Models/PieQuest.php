<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 30.07.2016
 * Time: 18:19
 */

namespace App\Modules\BlueBerryPie\Database\Models;


use App\Modules\PieBase\Database\Models\Base;

class PieQuest extends Base
{
    protected $perPage = 15;

    public $timestamps = true;
    protected $table = 'pie_quest';

    protected $fillable = array(
        'game_id',
        'is_first',
        'question',
        'status',
        'rule'
    );

    public function setIsFirstAttribute($value)
    {
        if ($value){
            $this->attributes['is_first'] = 1;
        } else {
            $this->attributes['is_first'] = 0;
        }
    }

    public function game()
    {
        return $this->belongsTo('\App\Modules\BlueBerryPie\Database\Models\PieGame','game_id');   
    }

    public function answer()
    {
        return $this->hasMany('\App\Modules\BlueBerryPie\Database\Models\PieAnswer','quest_id');
    }
    
    public function getInfo(){
        return [
            'id' => $this->id,
            'question' => $this->question,
            'is_first' => $this->is_first,
            'answer' => $this->answer->map(function($model){
                if($model->status == self::STATUS_ACTIVE)
                {
                    return $model->getInfo();
                }
            })->toArray()
        ];
    }
}