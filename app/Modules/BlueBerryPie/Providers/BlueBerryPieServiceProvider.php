<?php
namespace App\Modules\BlueBerryPie\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class BlueBerryPieServiceProvider extends ServiceProvider
{
	/**
	 * Register the BlueBerryPie module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\BlueBerryPie\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the BlueBerryPie module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('blue_berry_pie', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('blue_berry_pie', base_path('resources/views/vendor/blue_berry_pie'));
		View::addNamespace('blue_berry_pie', realpath(__DIR__.'/../Resources/Views'));
	}
}
