<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'berry'], function() {
    Route::group(['prefix' => 'api'], function() {
        $_prefix = 'api:';
        Route::group(['prefix' => 'q'], function() use ($_prefix) {
            $_action = 'question:';
            Route::any('/show/{id}', ['as' => $_prefix . $_action . 'index', 'uses' => 'Admin\ApiBlueBerryPieController@showPoint']);
            Route::any('/add', ['as' => $_prefix . $_action . 'add', 'uses' => 'Admin\ApiBlueBerryPieController@appendPoint']);
            Route::any('/remove/{id}', ['as' => $_prefix . $_action . 'index', 'uses' => 'Admin\ApiBlueBerryPieController@removePoint']);
            Route::any('/edit/{id}', ['as' => $_prefix . $_action . 'edit', 'uses' => 'Admin\ApiBlueBerryPieController@redoPoint']);
        });
        Route::group(['prefix' => 'a'], function() use ($_prefix) {
            $_action = 'answer:';
            Route::any('/show/{id}', ['as' => $_prefix . $_action . 'index', 'uses' => 'Admin\ApiBlueBerryPieController@showDirection']);
            Route::any('/add', ['as' => $_prefix . $_action . 'add', 'uses' => 'Admin\ApiBlueBerryPieController@appendDirection']);
            Route::any('/remove/{id}', ['as' => $_prefix . $_action . 'index', 'uses' => 'Admin\ApiBlueBerryPieController@removeDirection']);
            Route::any('/edit/{id}', ['as' => $_prefix . $_action . 'edit', 'uses' => 'Admin\ApiBlueBerryPieController@redoDirection']);
        });
    });
    Route::get('/{id}', ['as' => 'game:index', 'uses' => 'Admin\GameController@test']);
    Route::get('/walk/{id}', ['as' => 'game:walk', 'uses' => 'Admin\GameController@gq']);
});


