<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 30.07.2016
 * Time: 18:35
 */

namespace App\Modules\BlueBerryPie\Http\Controllers\Admin;


use App\Modules\BlueBerryPie\Http\Services\PieAnswer;
use App\Modules\BlueBerryPie\Http\Services\PieQuest;
use App\Modules\PieBase\Database\Models\Base;
use App\Modules\PieBase\Http\Controllers\Admin\Controller;
use Illuminate\Http\Request;

class ApiBlueBerryPieController extends Controller
{
    protected $prefix = 'game';
    protected $moduleName = 'blue_berry_pie';
    protected $serviceName = 'App\Modules\BlueBerryPie\Http\Services\PieGame';


    /*
     * Points
     */

    /**
     * @param Request $request
     * @param $id
     * @return \App\Modules\PieBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function showPoint(Request $request, $id)
    {
        if ($request->method() == 'POST') {
            return $this->sendWithErrors('Not available method');
        }
        $point = new PieQuest();
        $model = $point->getOne($id);
        if (!$model) {
            return $this->sendWithErrors('Not found question');
        }
        return $this->sendOk(['point' => $model->getInfo()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \App\Modules\PieBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function removePoint(Request $request, $id)
    {
        if ($request->method() != 'POST') {
            return $this->sendWithErrors('Not available method');
        }
        $point = new PieQuest();
        $model = $point->getOne($id);
        if (!$model) {
            return $this->sendWithErrors('Not found question');
        }
        $point->setStatus(Base::STATUS_DELETED);
        return $this->sendOk(['success' => 'Question deleted']);
    }

    /**
     * @param Request $request
     * @return \App\Modules\PieBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function appendPoint(Request $request)
    {
        if ($request->method() != 'POST') {
            return $this->sendWithErrors('Not available method');
        }

        $this->setRules([
            'question' => 'required|min:2',
            'game_id' => 'required|exists:pie_games,id',
            'is_first' => ''
        ]);

        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        $point = new PieQuest();
        if ($point->getModel()->fill($this->getRulesInput($request))->save()) {
            return $this->sendOk(['success' => 'Question Saved']);
        }
        return $this->sendWithErrors('Something went wrong');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \App\Modules\PieBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function redoPoint(Request $request, $id)
    {
        if ($request->method() != 'POST') {
            return $this->sendWithErrors('Not available method');
        }
        $this->setRules([
            'question' => 'required|min:2',
            'game_id' => 'required|exists:pie_games,id',
            'is_first' => ''
        ]);

        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }

        $point = new PieQuest();
        $model = $point->getOne($id);
        if ($model->fill($this->getRulesInput($request))->save()) {
            return $this->sendOk(['success' => 'Question Saved']);
        }
        return $this->sendWithErrors('Something went wrong');
    }

    /*
     * Directions
     */

    /**
     * @param Request $request
     * @param $id
     * @return \App\Modules\PieBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function showDirection(Request $request, $id)
    {
        if ($request->method() == 'POST') {
            return $this->sendWithErrors('Not available method');
        }
        $direction = new PieAnswer();
        $model = $direction->getOne($id);
        if (!$model) {
            return $this->sendWithErrors('Not found question');
        }
        return $this->sendOk(['point' => $model->getInfo()]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \App\Modules\PieBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function removeDirection(Request $request, $id)
    {
        if ($request->method() != 'POST') {
            return $this->sendWithErrors('Not available method');
        }
        $direction = new PieAnswer();
        $model = $direction->getOne($id);
        if (!$model) {
            return $this->sendWithErrors('Not found answer');
        }
        $direction->setStatus(Base::STATUS_DELETED);
        return $this->sendOk(['success' => 'Answer deleted']);
    }

    /**
     * @param Request $request
     * @return \App\Modules\PieBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function appendDirection(Request $request)
    {
        if ($request->method() != 'POST') {
            return $this->sendWithErrors('Not available method');
        }

        $this->setRules([
            'answer' => 'required|min:2',
            'quest_id' => 'required|exists:pie_quest,id',
            'answer_id' => 'required|exists:pie_quest,id',
        ]);

        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        $direction = new PieAnswer();
        if ($direction->getModel()->query()->where('quest_id', $request->input('quest_id'))
            ->where('answer_id', $request->input('answer_id'))->first()
        ) {
            return $this->sendWithErrors('Answer already exist');
        }
        $front = $direction->getModel()->fill(
            ['answer' => $request->input('answer'),
                'quest_id' => $request->input('quest_id'),
                'answer_id' => $request->input('answer_id'),
                'value' => 1]
        )->save();
        $direction = new PieAnswer();
        if ($direction->getModel()->query()->where('quest_id', $request->input('answer_id'))
            ->where('answer_id', $request->input('quest_id'))->first()
        ) {
            return $this->sendWithErrors('Answer already exist');
        }
        $back = $direction->getModel()->fill([
            'answer' => 'Back',
            'quest_id' => $request->input('answer_id'),
            'answer_id' => $request->input('quest_id'),
            'value' => -1

        ])->save();
        if ($front && $back) {
            return $this->sendOk(['success' => 'Answer Saved']);
        }
        return $this->sendWithErrors('Something went wrong');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \App\Modules\PieBase\Http\Requests\Response|\Illuminate\Http\Response
     */
    public function redoDirection(Request $request, $id)
    {
        if ($request->method() != 'POST') {
            return $this->sendWithErrors('Not available method');
        }

        $this->setRules([
            'answer' => 'required|min:2',
            'quest_id' => 'required|exists:pie_quest,id',
            'answer_id' => 'required|exists:pie_quest,id',
        ]);

        if ($this->isValidationFails($request)) {
            return $this->sendWithErrors($this->getValidatorErrors());
        }
        $direction = new PieAnswer();
        $model = $direction->getOne($id);
        if ($model->fill($this->getRulesInput($request))->save()) {
            return $this->sendOk(['success' => 'Answer Saved']);
        }
        return $this->sendWithErrors('Something went wrong');
    }
}